class Bee < ActiveRecord::Base
  has_many :harvests
  has_many :pollens, through: :harvests
end
