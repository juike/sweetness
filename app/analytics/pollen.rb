module Analytics
  module Pollen
    extend self

    def most_contributed
      analytic_scope.select('pollens.*, sum(sugar * value) AS contribute').order('contribute DESC')
    end

    # Total number of days in which bees harvested the pollen
    def popularity_by_days
      analytic_scope.select('pollens.*, count(*) AS popularity').order('popularity DESC')
    end

    # Total number of harvested pollen (not sugar!)
    def popularity_by_harvest
      analytic_scope.select('pollens.*, sum(value) AS popularity').order('popularity DESC')
    end

    # quantity of bees preferring the certain pollen
    def popularity_by_bees
      ::Pollen.find_by_sql <<-SQL
        WITH preferred_pollens AS (
          SELECT bee_id, pollen_id, count(*) days
          FROM harvests
          GROUP BY bee_id, pollen_id
        ), ranged_prepeffered_pollens AS (
          SELECT bee_id, pollen_id, days, rank() OVER w, count(*) OVER (PARTITION BY bee_id, days)
          FROM preferred_pollens
          WINDOW w AS (PARTITION BY bee_id ORDER BY days DESC)
        )
        SELECT pollens.*, sum(1.0 / count)::NUMERIC(9, 3) AS popularity
        FROM ranged_prepeffered_pollens
        JOIN pollens ON id = pollen_id
        WHERE rank = 1
        GROUP BY pollens.id
        ORDER BY popularity DESC;
      SQL
    end

    private
    def analytic_scope
      ::Pollen.joins(:harvests).group('pollens.id')
    end
  end
end
