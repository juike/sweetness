module Analytics
  module Harvest
    extend self

    def total_sugar_per_day(order = :day)
      order_by = case order
                 when :day then '2'
                 when :sugar then '1'
                 else
                   raise ArgumentError.new("Unknown order: #{order.inspect}")
                 end
      total_sugar(analytic_scope.order(order_by))
    end

    def best_day
      total_sugar(analytic_scope.order('1 DESC').limit(1))
    end

    def worst_day
      total_sugar(analytic_scope.order('1').limit(1))
    end

    private

    def analytic_scope
      ::Harvest.joins(:pollen).group('harvests.day')
    end

    def total_sugar(scope)
      scope.sum('harvests.value * pollens.sugar')
    end
  end
end
