\set dbname `echo ${db:=sweetness}`
\set harvest `echo $(pwd)/${harvest:=harvest.csv}`
\set pollens `echo $(pwd)/${pollens:=pollens.csv}`

drop database if exists :dbname;
create database :dbname with template = template1;
\c :dbname
create table harvests (id serial primary key, bee_id integer, day date, pollen_id integer, value numeric(4,1));
create table pollens(id serial primary key, name varchar, sugar integer);
create table bees(id serial primary key);

copy harvests (bee_id, day, pollen_id, value) from :'harvest' with (format csv, header true);
copy pollens (id, name, sugar) from :'pollens' with (format csv, header true);
insert into bees select distinct bee_id from harvests;

/*
\echo Which pollen contributed the most sugar in total?
select p.name, sum(h.value * p.sugar) as "Sugar in total" from pollens p join harvests h on h.pollen_id = p.id group by p.name order by 2 desc;

\echo Which day was the best day for harvesting? Which one was the worst?
\echo Table of total sugar harvested per day.
select h.day, sum(h.value * p.sugar) as "sugar harvested per day" from pollens p join harvests h on h.pollen_id = p.id group by h.day order by 1;
\echo Table of total sugar harvested per day. (order by total sugar)
select h.day, sum(h.value * p.sugar) as "sugar harvested per day" from pollens p join harvests h on h.pollen_id = p.id group by h.day order by 2 desc;

\echo Which pollen was most popular among the bees?
\echo By quantity of days in which bees prefer the pollen
select p.name, count(*) as "all bees-days" from pollens p join harvests h on h.pollen_id = p.id group by p.name order by 2 desc;

\echo By total harvest of the pollen of all the bees
select p.name, sum(h.value) as "total harvests" from pollens p join harvests h on h.pollen_id = p.id group by p.name order by 2 desc;

\echo By quantity of bees preferring the certain pollen
select p.name, count(*) bees_count from (
  select distinct bee_id, first_value(pollen_id) over (partition by bee_id order by count desc) as pollen_id from (
    select bee_id, pollen_id, count(*) from harvests group by bee_id, pollen_id
  ) _
) _ join pollens p on pollen_id = p.id
group by p.name order by bees_count desc;

\echo Which bee was most effective (udarnik)? Which one was the least?
\echo Table of all effectiveness
select h.bee_id, avg(h.value * p.sugar)::numeric(6, 2) effectiveness from harvests h join pollens p on h.pollen_id = p.id group by bee_id order by effectiveness desc;
*/
