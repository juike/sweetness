module Analytics
  module Bee
    extend self

    def effectiveness
      ::Bee.select('bees.*, AVG(value * sugar)::numeric(7, 3) AS effectiveness')
           .joins(:pollens)
           .group('bees.id')
           .order('effectiveness DESC')
    end
  end
end
