require 'spec_helper'

describe Analytics::Pollen do
  describe '.most_contributed' do
    # 9 * (23.3 + 17.3 + 22.3 + 24.5 + 16.3) = 933.3
    # 30 * (22.8 + 40.8 + 12.1 + 42.3) = 3540
    let(:pollens) { subject.most_contributed }

    it 'returns all pollens ordered by total contributed' do
      expect(pollens[0].name).to eq('Bluebell')
      expect(pollens[1].name).to eq('Canola')
    end

    it 'calculates right total contributed' do
      expect(pollens[0].contribute).to eq(BigDecimal.new(3540))
      expect(pollens[1].contribute).to eq(BigDecimal.new(933.3, 4))
    end
  end

  describe '.populatity_by_days' do
    # 1 2013-04-02, 2 2013-04-03, 3 2013-04-01, 3 2013-04-02, 3 2013-04-04
    # 1 2013-04-01, 1 2013-04-04, 2 2013-04-04, 3 2013-04-03
    let(:pollens) { subject.popularity_by_days }

    it 'returns pollens ordered by popularity' do
      expect(pollens.map(&:id)).to eq([1, 2])
    end

    it 'returns right popularity' do
      expect(pollens.map(&:popularity)).to eq([5, 4])
    end
  end

  describe '.popularity_by_harvest' do
    # 23.3 + 17.3 + 22.3 + 24.5 + 16.3 = 103.7
    # 22.8 + 40.8 + 12.1 + 42.3 = 118
    let(:pollens) { subject.popularity_by_harvest }

    it 'returns ordered pollens' do
      expect(pollens.map(&:id)).to eq([2, 1])
    end

    it 'returns right popularity' do
      expect(pollens.map(&:popularity)).to eq([BigDecimal.new(118, 4), BigDecimal.new(103.7, 4)])
    end
  end

  describe '.popularity_by_bees' do
    # bees prefer:
    # 1p -> 04-02, 2p -> 04-01, 04-03 => 2p has 1 score
    # 1p -> 04-03, 2p -> 04-04 => 1p, 2p have 0.5 score
    # 1p -> 01, 02, 04, 2p -> 03 => 1p has 1 score
    it 'calculates population' do
      pollens = subject.popularity_by_bees
      expect(pollens.map(&:popularity)).to eq([BigDecimal.new(1.5, 2), BigDecimal.new(1.5, 2)])
    end

    it 'returns ordered pollens' do
      Harvest.create(day: Date.parse('2013-04-02'), bee_id: 2, pollen_id: 2, value: 17)
      pollens = subject.popularity_by_bees
      expect(pollens.map(&:id)).to eq([2, 1])
      expect(pollens.map(&:popularity)).to eq([BigDecimal.new(2), BigDecimal.new(1)])
      Harvest.last.destroy
    end
  end
end
