require 'spec_helper'

describe Analytics::Bee do
  describe '.effectiveness' do
    # (22.8 * 30 + 23.3 * 9 + 40.8 * 30) / 3 = 705.9
    # (17.3 * 9 + 12.1 * 30) / 2 = 259.35
    # ((22.3 + 24.5 + 16.3) * 9 + 42.3 * 30) / 4 = 459.225
    let(:bees) { subject.effectiveness }

    it 'returns all bees orderer by effectiveness' do
      expect(bees.map(&:id)).to eq([1, 3, 2])
    end

    it 'returns right effectiveness' do
      expected = [705.9, 459.225, 259.35].map { |x| BigDecimal.new(x, 6) }
      expect(bees.map(&:effectiveness)).to eq(expected)
    end
  end
end
