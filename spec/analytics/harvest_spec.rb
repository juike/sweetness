require 'spec_helper'

describe Analytics::Harvest do
  describe ".total_sugar_harvested_per_day" do
    # 1 => 22.8 * 30 + 22.3 * 9 = 884.7
    # 2 => 23.3 * 9 + 24.5 * 9 = 430.2
    # 3 => 17.3 * 9 + 42.3 * 30 = 1424.7
    # 4 => 40.8 * 30 + 12.1 * 30 + 16.3 * 9 = 1733.7
    context "when order by day" do
      it 'returns hash: day => total sugar ordered by days' do
        data = subject.total_sugar_per_day
        days = %w|2013-04-01 2013-04-02 2013-04-03 2013-04-04|.map { |s| Date.parse(s) }
        values = [884.7, 430.2, 1424.7, 1733.7].map { |x| BigDecimal.new(x, 5) }
        expected = Hash[days.zip(values)]
        expect(data).to eq(expected)
      end
    end

    context "when order by sugar" do
      it 'returns hash: day => total sugar ordered by sugar' do
        data = subject.total_sugar_per_day
        days = %w|2013-04-04 2013-04-03 2013-04-01 2013-04-02|.map { |s| Date.parse(s) }
        values = [1733.7, 1424.7, 884.7, 430.2].map { |x| BigDecimal.new(x, 5) }
        expected = Hash[days.zip(values)]
        expect(data).to eq(expected)
      end
    end

    context "when unknown order parameter" do
      it 'raises error' do
        expect { subject.total_sugar_per_day(:honey) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '.best_day' do
    it 'returns the best day' do
      expect(subject.best_day).to eq({ Date.parse('2013-04-04') => BigDecimal.new(1733.7, 5) })
    end
  end

  describe '.worst_day' do
    it 'returns the worst day' do
      expect(subject.worst_day).to eq({ Date.parse('2013-04-02') => BigDecimal.new(430.2, 5) })
    end
  end
end
