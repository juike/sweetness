ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'rubygems'
require 'bundler/setup'
require 'yaml'

Bundler.require

db_config = YAML.load_file("config/database.yml")[ENV['sweetness'] || 'test']

ActiveRecord::Base.establish_connection(db_config)
#ActiveRecord::Base.logger = Logger.new(STDOUT)

Dir[File.expand_path('../../app/**/*.rb', __FILE__)].each do |file|
  require file
end
