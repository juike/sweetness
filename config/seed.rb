require 'csv'
require_relative './environment'

bees_count = 10000
HARVEST = 20
days = (Date.parse('2013-04-01')..Date.parse('2013-08-31'))

def rand_sign
  rand(2).zero? ? -1 : 1
end

pollens = Pollen.all.map do |pollen|
  def pollen.labor_contribution
    1 + (rand * rand_sign / 3)
  end
  pollen
end

class BeeTest < Struct.new(:id, :capacity_for_work, :effectiveness)
  def self.pollens=(pollens)
    @pollens = pollens.reduce({}) { |res, pollen| res[pollen.id] = pollen; res }
  end

  def self.pollens
    @pollens
  end

  def initialize(*)
    super
    set_preferred_pollens
    @works = 0
  end

  def can_work?
    @works < capacity_for_work
  end

  def day_off
    @works = 0
  end

  def work(day)
    pollen = random_pollen
    harvest = HARVEST * [capacity_for_work - @works, 1].min * effectiveness / pollen.labor_contribution
    @works += 1
    [id, day, pollen.id, harvest]
  end

  def random_pollen
    pollen_id = @preferred_pollens.sample
    self.class.pollens[pollen_id]
  end

  private
  def set_preferred_pollens
    @preferred_pollens = self.class.pollens.values.map do |pollen|
      [pollen.id] * rand(10)
    end.flatten
  end
end

BeeTest.pollens = pollens

bees = bees_count.times.map do |id|
  BeeTest.new(id + 1, 3 + rand * rand_sign, 1 + rand * rand_sign / 2)
end

CSV.open('big-hive.csv', 'w') do |csv|
  days.each do |day|
    bees.each do |bee|
      if bee.can_work?
        csv << bee.work(day)
      else
        bee.day_off
      end
    end
  end
end
